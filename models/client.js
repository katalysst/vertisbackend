
const bcrypt = require("bcryptjs");

module.exports = function(sequelize, DataTypes) {
  const Client = sequelize.define("Client", {
    ClientID: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    }, 
    
    ClientName: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    ManagerName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    ManagerPosition: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    freezeTableName: true
  });

  return Client;
};
 