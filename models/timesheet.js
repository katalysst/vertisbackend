const bcrypt = require("bcryptjs");

module.exports = function(sequelize, DataTypes) {
  const Timesheet = sequelize.define("Timesheet", {
    TimesheetID: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    }, 
    ClientID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Client',
        key: 'ClientID'
      }
    },
    EmployeeID: { 
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Employee',
        key: 'EmployeeID'
      }
    },
    WorkTitle: { 
      type: DataTypes.STRING,
      allowNull: false
    },
    Date: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    StartTime: {
        type: DataTypes.TIME, 
        allowNull: false
    },
    EndTime: {
        type: DataTypes.TIME,
        allowNull: false
    },
    Comments: {
        type: DataTypes.TEXT,
        allowNull: false
    }
  }, {
    freezeTableName: true
  });
 
  return Timesheet;
};
