
const bcrypt = require("bcryptjs");
module.exports = (sequelize, DataTypes) => {
  const Employee = sequelize.define("Employee", {
    EmployeeID: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    }, 
    UserName: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    FirstName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    LastName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    Address: {
      type: DataTypes.STRING,
      allowNull: false
    },

    Email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true      
    },
    Password: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    freezeTableName: true
  });



  return Employee;
};
