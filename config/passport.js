const bcrypt = require('bcrypt');
const Sequelize = require('sequelize');
const jwtSecret = require('./jwtConfig');
const BCRYPT_SALT_ROUNDS = 12;
const Op = Sequelize.Op;

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const db = require("../models");


passport.use( 'login', new LocalStrategy(
  {
    usernameField: 'UserName',
    passwordField: 'Password'
  }, (UserName, password, done) => {

      try {

        db.Employee.findOne({where: {UserName}}).then(employee => {

          if (employee === null) {
            return done(null, false, { message: 'Username not found' });
          }
          
          bcrypt.compare(password, employee.Password).then(response => {
            if (response !== true) {
              return done(null, false, { message: 'Passwords do not match' });
            }
            return done(null, employee);
          });

        });

      } catch (err) {
        done(err);
      }

    },
  ),
);

const opts = {
  jwtFromRequest: ExtractJWT.fromAuthHeaderWithScheme('JWT'),
  secretOrKey: jwtSecret.secret,
};

passport.use( 'jwt', new JWTstrategy(opts, (jwt_payload, done) => {

  console.log(jwt_payload)
    try {

      db.Employee.findOne({
        where: {
          EmployeeID: jwt_payload.id,
        },
      }).then(employee => {

        if (employee) {
          console.log('Employee Found');
          done(null, employee);
        } else {
          console.log('Employee Not Found');
          done(null, false);
        }

      });

    } catch (err) {
      done(err);
    }
  }),
);
