const express = require("express");
const bodyParser = require("body-parser");
const session = require("express-session");
const passport = require('passport');
const Initialize = require('./controllers/initializeDb');
var cors = require('cors');

const PORT = process.env.PORT || 8080;

const db = require("./models");

const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false })); 
app.use(bodyParser.json());
app.use(express.static("public"));

require('./config/passport')

app.use(session({ secret: "keyboard cat", resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());


require("./routes/routes.js")(app);

db.sequelize.sync().then(function() {
  Initialize.SetupClients()
  Initialize.SetupEmployees();
  Initialize.setUpProcedure();

  app.listen(PORT, function() {
    console.log("==> 🌎  Listening on port %s. Visit http://localhost:%s/ in your browser.", PORT, PORT);
  });
});