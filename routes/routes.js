const jwt = require('jsonwebtoken');
const passport = require('passport');
const jwtSecret = require('../config/jwtConfig');
const db = require("../models");
const moment = require('moment');
let date = new Date(Date.now());
date = moment(date).format("YYYY-MM-DD HH:mm:ss");

module.exports = function(app) {
  
  app.post('/api/login', (req, res, next) => {
    console.log(req.body)
      passport.authenticate('login', (err, users, info) => {
      
      if (err) {
        console.error(`error ${err}`);
      }
      if (info !== undefined) {
        console.error(info.message);
        if (info.message === 'bad username') {
          res.status(401).send(info.message);
        } else {
          res.status(403).send(info.message);
        }
      } else {
        req.logIn(users, () => {
          db.Employee.findOne({
            where: {
              UserName: req.body.UserName,
            },
          }).then(employee => {
            const token = jwt.sign({ id: employee.EmployeeID },  jwtSecret.secret, {
              expiresIn: 1.21e+6,
            });
            res.send({
              status: 200,
              auth: true,
              token,
              employee: {
                employeeID: employee.EmployeeID,
                firstName: employee.FirstName,
                lastName: employee.LastName,
                address: employee.Address
              },
              message: 'Login Successful',
            });
          });
        });
      }
    })(req, res, next);
   
  });

  app.get("/api/clients", (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
      if (err) {
        console.log(err);
      }
      if (info !== undefined) {
        console.log(info.message);
        res.status(401).send(info.message);
      } else if (user.username === req.query.username) {
        db.Client.findAll().then((clients) => {
          if (clients != null) {
            res.send({
              status: 200,
              auth: true,
              clients
            });
          } else {
            console.error('no user exists in db with that username');
            res.status(401).send('no user exists in db with that username');
          }
        });
      } else {
        console.error('jwt id and username do not match');
        res.status(403).send('username and jwt token do not match');
      }
    })(req, res, next);

    
  });


  app.post("/api/timesheet", (req, res, next) => {
    console.log(req.body)
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
      if (err) {
        console.log(err);
      }
      if (info !== undefined) {
        console.log(info.message);
        res.status(401).send(info.message);
      } else if (user.username === req.query.username) {
        db.sequelize.query(`CALL insertTimesheet(${req.body.clientID}, ${req.body.employeeID}, '${req.body.workTitle}', 
        '${req.body.date}', '${req.body.startTime}', '${req.body.endTime}', '${req.body.comments}', '${date}', '${date}')`).then(timesheet => {
          console.log(timesheet)
          
            res.send({
              status: 200,
              auth: true,
              message: 'Timesheet created'
            });
         
        });
      } else {
        console.error('jwt id and username do not match');
        res.status(403).send('username and jwt token do not match');
      }
    })(req, res, next);

  });

  app.get("/api/hi", function(req, res) {
    console.log('ha')
  });
//
  
};