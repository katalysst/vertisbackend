const db = require('./../models');
const bcrypt = require('bcrypt');
const moment = require('moment');
let date = new Date(Date.now());
date = moment(date).format("YYYY-MM-DD HH:mm:ss");

exports.setUpProcedure = async(req, res) => {
    try {
        db.sequelize.query(`
        CREATE PROCEDURE insertTimesheet(ClientID INT, EmployeeID INT, WorkTitle VARCHAR(255), Date DATE, StartTime TIME,
            EndTime TIME, Comments LONGTEXT, createdAt DATE, updatedAt DATE)
        BEGIN
            INSERT INTO TIMESHEET(ClientID, EmployeeID, WorkTitle, Date, StartTime, EndTime, Comments, createdAt, updatedAt)
            VALUES(ClientID, EmployeeID, WorkTitle, Date, StartTime, EndTime, Comments, createdAt, updatedAt);
        END;`) 
    } catch(err) {
        console.log(err);
    }
} 
     
exports.SetupClients = async(req, res) => {
 
    try {
         const clientsInsert = await db.sequelize.query('INSERT INTO Client(ClientName, ManagerName, ManagerPosition, createdAt, updatedAt) VALUES (?), (?), (?), (?), (?);', {
            replacements: [
                ['Amazon', 'Bryan Quinn', 'Project Manager', date, date], 
                ['Adobe', 'Collin Mocherie', 'Scrum Master', date, date],
                ['Jet Blue', 'Cyril Figgis', 'Vice Pesident of Accounting', date, date],
                ['Ebay', 'Archer Sterling', 'Vice President of Technology', date, date],
                ['Digicel', 'Lana Kane', 'CEO', date, date]
            ], 
            type: db.sequelize.QueryTypes.INSERT
        });

    } catch (err) {
        console.log(err)
    }
    
}

exports.SetupEmployees = async(req, res) => {
    let date = new Date(Date.now());
    date = moment(date).format("YYYY-MM-DD HH:mm:ss");

    try {
        db.Employee.beforeCreate(employee => {
            employee.Password = bcrypt.hashSync(
              employee.Password,
               bcrypt.genSaltSync(10),
               null
             ); 
           });      

        db.Employee.bulkCreate([
            {
                'UserName': 'kbrown',
                'FirstName': 'Khadijah', 
                'LastName': 'Brown', 
                'Address': '21 Johnson Terrace Kingston 2', 
                'Email': 'kbrown@vertisjm.com', 
                'Password': bcrypt.hashSync(
                    'test@23aL',
                     bcrypt.genSaltSync(10),
                     null
                   )
            },
            {
                'UserName': 'rsterling',
                'FirstName': 'Ryan', 
                'LastName': 'Sterling', 
                'Address': '35 Jackson Road, Kingston 10', 
                'Email': 'rsterling@vertisjm.com', 
                'Password': bcrypt.hashSync(
                    'sheetTest@aP',
                     bcrypt.genSaltSync(10),
                     null
                   )
            },
            { 
                'UserName': 'mhanson',
                'FirstName': 'Matthew', 
                'LastName': 'Hanson', 
                'Address': '22 LilyLake Avenue, Kingston 6', 
                'Email': 'mhanson@vertisjm.com', 
                'Password': bcrypt.hashSync(
                    'hanshew@aP',
                     bcrypt.genSaltSync(10),
                     null
                   )
            },
            {
                'UserName': 'sspencer',
                'FirstName': 'Shawn', 
                'LastName': 'Spencer', 
                'Address': '53 SantaBarbara Boulevard, Kingston 12', 
                'Email': 'sspencer@vertisjm.com', 
                'Password': bcrypt.hashSync(
                    'tesla23!',
                     bcrypt.genSaltSync(10),
                     null
                   )
            }, 
            {
                'UserName': 'gburton',
                'FirstName': 'Gus', 
                'LastName': 'Burton', 
                'Address': '44 TTShowbiz Lane, Kingston 2', 
                'Email': 'gburton@vertisjm.com', 
                'Password': bcrypt.hashSync(
                    'gylenhaulT2',
                     bcrypt.genSaltSync(10),
                     null
                   )
            },
        ]).catch(function(err) {
            console.log(err);
        });

    } catch (err) {
        console.log(err)
    }
    
}



